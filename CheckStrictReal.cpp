#include "CheckStrictReal.h"

#include <future>
#include <vector>

#include "Enumerator.h"
#include "UTMatrix.h"

namespace strict_real {

void check_thread(async::safe_queue<strict_real::matr_n_inv>& out,
                  std::deque<bit_dump>& matrices,
                  std::deque<bit_dump>& involutions,
                  size_t step, size_t offset)
{
	ut::ut identity;
	ut::set(identity, ut::set_identity_ut);

	ut::ut inv;
	ut::set(inv, ut::set_identity_ut);
	enm::enumerator inv_enm;
	for (int i_ = size - 1; i_ > 0; i_--) {
		for (int j_ = i_ - 1; j_ >= 0; j_--) {
			inv_enm.positions.push_back({
				                               .i = i_,
				                               .j = j_,
				                               .value = &inv[i_][j_],
			                               });
		}
	}

	ut::ut x;
	ut::set(x, ut::set_identity_ut);

	enm::enumerator x_enm;
	for (int i_ = size - 1; i_ > 0; i_--) {
		for (int j_ = i_ - 1; j_ >= 0; j_--) {
			x_enm.positions.push_back({
				                            .i = i_,
				                            .j = j_,
				                            .value = &x[i_][j_],
			                            });
		}
	}

	ut::ut ix;

	for (size_t i = offset; i < matrices.size(); i += step) {
		x_enm.from_bits(matrices[i]);
		inv = identity;
		ix = x; // identity * x
		bool found = false;
		for (const auto& inv_bits : involutions) {
			inv_enm.from_bits(inv_bits);
			ut::mult_l_constrained(inv, inv_enm.i_flags, x, ix);
			if (ut::is_involution(ix)) {
				out.push(std::make_pair(matrices[i], inv_bits));
				found = true;
				break;
			}
		}
		if (!found) {
			out.push(std::make_pair(matrices[i], bit_dump{0}));
		}
	}
}

void check(async::safe_queue<strict_real::matr_n_inv>& out, std::deque<bit_dump>& matrices, std::deque<bit_dump>& involutions, size_t th_count)
{
	std::vector<std::future<void>> thread_results;
	thread_results.reserve(th_count);

	for (size_t i = 0; i < th_count; i++) {
		thread_results.push_back(std::async(check_thread, std::ref(out), std::ref(matrices), std::ref(involutions), th_count, i));
	}
}

}

