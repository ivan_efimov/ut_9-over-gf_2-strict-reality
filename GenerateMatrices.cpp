#include "GenerateMatrices.h"

#include "Enumerator.h"
#include "UTMatrix.h"

namespace gm {

using sub_M_t = std::vector<int>;
using M_t = std::vector<sub_M_t>;

void build_M(const enm::enumerator& e, M_t& out) {
	bool in_continuous = false;
	sub_M_t sub_M;

	for (int k = 2; k <= size; k++) {
		if (*e.positions[k - 2].value != 0) {
			in_continuous = true;
			sub_M.push_back(k);
		} else {
			if (in_continuous) {
				out.push_back(std::move(sub_M));
				sub_M.clear();
			}
			in_continuous = false;
		}
	}
	if (in_continuous) {
		out.push_back(std::move(sub_M));
		sub_M.clear();
	}
}

bool in_M(const M_t& M, int i) {
	for (const auto& sub_M : M) {
		if (i >= sub_M.front() && i <= sub_M.back()) {
			return true;
		}
	}
	return false;
}

bool is_max(const M_t& M, int i) {
	for (auto& sub_M : M) {
		if (i == sub_M.back()) {
			return true;
		}
	}
	return false;
}

size_t generate(std::deque<bit_dump>& out, const std::unordered_set<bit_dump>& already_done)
{
	size_t count = 0;

	ut::ut m;
	ut::set(m, ut::set_identity_ut);

	enm::enumerator sub_diag;
	for (int i = 1; i < size; i++) {
		sub_diag.positions.push_back({
			                             .i = i,
			                             .j = i - 1,
			                             .value = &m[i][i - 1]
		                             });
	}

	sub_diag.increase(1); // Пропускаем случай с нулевой поддиагональю

	enm::enumerator m_dumper;
	for (int i = size - 1; i > 0; i--) {
		for (int j = i - 1; j >= 0; j--) {
			m_dumper.positions.push_back({
				                             .i = i,
				                             .j = j,
				                             .value = &m[i][j],
			                             });
		}
	}

	enm::enumerator significant_elems;


	M_t M;
	do {
		M.clear();
		build_M(sub_diag, M);

		significant_elems.positions.clear();

		for (int i = 2; i <= size; i++) {
			for (int j = 1; j < i - 1; j++) {
				if ((in_M(M, j + 1) && i > j + 1) || (in_M(M, i) && i > j + 1 && !is_max(M, j))) {
					m[i - 1][j - 1] = 0;
				} else {
					m[i - 1][j - 1] = 0;
					significant_elems.positions.push_back({
						.i = i - 1,
						.j = j - 1,
						.value = &m[i - 1][j - 1]
					});
				}
			}
		}

		bit_dump tmp;
		do {
			tmp = m_dumper.to_bits();
			if (!already_done.contains(tmp)) {
				out.push_back(tmp);
				count++;
			}
		} while (significant_elems.increase(1));


	} while (sub_diag.increase(1));

	return count;
}

}