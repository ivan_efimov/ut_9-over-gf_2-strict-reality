#pragma once

#include <array>
#include <bitset>
#include <functional>
#include <cstddef>
#include <cstdint>
#include <cstring>

#include "Consts.h"

namespace ut {

using ut = std::array<std::array<elem_t, size>, size>;

void set(ut& out, const std::function<elem_t(size_t, size_t)>& f);
elem_t set_identity_ut(size_t i, size_t j);

void mult_l_constrained(const ut& a, const std::bitset<size>& rows_changed, const ut& b, ut& out);

bool is_involution(const ut& a);

}
