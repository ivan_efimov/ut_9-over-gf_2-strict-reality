#pragma once

#include <bitset>
#include <vector>

#include "Consts.h"

namespace enm {

struct pos_value_t {
	int i;
	int j;

	elem_t* value;

	elem_t increase(elem_t step) {
		*value += step;
		if (*value > max_value) {
			elem_t carry = *value / (max_value + 1);
			*value %= (max_value + 1);
			return carry;
		}
		return 0;
	}
};

struct enumerator {
	std::vector<pos_value_t> positions;
	std::bitset<size> i_flags;
	std::bitset<size> j_flags;

	bool increase(elem_t step) {
		elem_t carry = step;
		i_flags.reset();
		j_flags.reset();
		elem_t old_value;
		for (auto& pos : positions) {
			old_value = *pos.value;
			carry = pos.increase(carry);
			if (old_value != *pos.value) {
				i_flags.set(pos.i);
				j_flags.set(pos.j);
			}
			if (carry == 0) {
				return true;
			}
		}
		return false;
	}

#if GF_CHAR == 2
	[[nodiscard]] bit_dump to_bits() const {
		bit_dump d;
		for (size_t i = 0; i < positions.size(); i++) {
			d[i] = *positions[i].value != elem_t(0);
		}
		return d;
	}

	void from_bits(const bit_dump& d) {
		i_flags.reset();
		j_flags.reset();
		elem_t new_value;
		for (size_t i = 0; i < positions.size(); i++) {
			new_value = d[i] ? elem_t(1) : elem_t(0);
			if (new_value != *positions[i].value) {
				i_flags.set(positions[i].i);
				j_flags.set(positions[i].j);
				*positions[i].value = new_value;
			}
		}
	}
#endif
};

}
