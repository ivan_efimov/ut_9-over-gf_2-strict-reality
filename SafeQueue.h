#pragma once

#include <condition_variable>
#include <deque>
#include <mutex>
#include <type_traits>

namespace async {

template<typename T>
class safe_queue {
public:
	/// Pushes a value to queue without blocking.
	/// @attention Does nothing if queue is closed.
	template<typename V>
	void push(V&& v) {
		std::unique_lock<std::mutex> lock(mutex_);
		if (!is_open_) {
			return;
		}

		list_.push_back(std::forward<V>(v));
		if (list_.size() == 1) {
			// There may be someone waiting new value
			cv_.notify_one();
		}
	}

	/// @attention If queue is empty, block until a next value appeared or queue has been closed.
	/// @returns {T{}, false} if queue is closed, {`next value`, true} otherwise.
	std::tuple<T, bool> pop() {
		T v;
		{
			std::unique_lock<std::mutex> lock(mutex_);

			while (list_.empty() && is_open_) {
				cv_.wait(lock);
			}

			if (list_.empty() && !is_open_) {
				return {T{}, false};
			}

			std::swap(v, list_.front());
			list_.pop_front();
		}
		return {std::move(v), true};
	}

	/// Same as push(...), but doesn't block, so
	/// @returns {T{}, false} either queue is closed or empty, {`value of T`, true} otherwise.
	std::tuple<T, bool> pop_non_blocking() {
		T v;
		{
			std::unique_lock<std::mutex> lock(mutex_);

			if (!is_open_ || list_.empty()) {
				return {T{}, false};
			}

			std::swap(v, list_.front());
			list_.pop_front();
		}
		return {v, true};
	}

	void close() {
		std::unique_lock<std::mutex> lock(mutex_);
		is_open_ = false;

		cv_.notify_all();
	}

private:
	bool is_open_ = true;

	std::condition_variable cv_;
	std::mutex mutex_;

	std::deque<T> list_;
};

}