#include <iostream>
#include <chrono>
#include <fstream>
#include <sstream>
#include <unordered_set>
#include <future>
#include <csignal>

#include "GenerateInvolutions.h"
#include "GenerateMatrices.h"
#include "CheckStrictReal.h"

void sigh(int sig) {

}

int main(int argc, char** argv)
{
	if (argc != 2) {
		std::cout << "invalid arguments!\n";
		exit(1);
	}
	std::stringstream ss(argv[1]);
	int thread_count;
	ss >> thread_count;
	if (thread_count <= 0) {
		std::cout << "invalid arguments!\n";
		exit(1);
	}
	std::cout << "running with " << thread_count << " threads...\n";

	signal(SIGINT, sigh);

	std::unordered_set<bit_dump> already_done;

	std::ifstream in("report_old.txt");
	bit_dump _m, _inv;
	while (in) {
		in >> _m >> _inv;
		if (in.eof()) {
			break;
		}
		already_done.insert(_m);
	}
	std::cout << "Restored " << already_done.size() << " matrices\n";
	in.close();

	std::deque<bit_dump> m_s;
	std::deque<bit_dump> involutions;

	std::cout << std::boolalpha;

	std::cout << "Generating matrices..." << std::endl;
	auto start_time = std::chrono::system_clock::now();
	auto m_count = gm::generate(m_s, already_done);
	auto finish_time = std::chrono::system_clock::now();
	auto elapsed = finish_time - start_time;
	std::cout << "Done! Elapsed time: " << std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count() << " ms" << std::endl;
	std::cout << "Generated: " << m_s.size() << " (" << (already_done.size() + m_s.size()) << " total)" << std::endl;

	std::cout << "Generating involutions..." << std::endl;
	start_time = std::chrono::system_clock::now();
	auto inv_count = gi::generate(involutions, thread_count);
	finish_time = std::chrono::system_clock::now();
	elapsed = finish_time - start_time;
	std::cout << "Done! Elapsed time: " << std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count() << " ms" << std::endl;
	std::cout << "Generated: " << involutions.size() << std::endl;

	async::safe_queue<strict_real::matr_n_inv> write_queue;
	size_t matrices_ready = already_done.size();
	size_t matrices_total = (already_done.size() + m_s.size());
	size_t a_thousandth = matrices_total / 1000;
	size_t last_thousandths = 0;
	size_t bad_count = 0;


	auto check_future = std::async(std::launch::async, strict_real::check, std::ref(write_queue), std::ref(m_s), std::ref(involutions),
	                               thread_count);

	std::ofstream out("report.txt");
	std::ofstream bad("bad.txt");


	std::cout << "Checking matrices..." << std::endl;
	start_time = std::chrono::system_clock::now();
	while (true) {
		if (matrices_ready == matrices_total) {
			break;
		}

		size_t thousandths = matrices_ready / a_thousandth;
		if (thousandths != last_thousandths) {
			last_thousandths = thousandths;
			finish_time = std::chrono::system_clock::now();
			elapsed = finish_time - start_time;
			std::cout << "[" << std::chrono::duration_cast<std::chrono::seconds>(elapsed).count() << " s]";
			std::cout.flush();
			if (thousandths % 10 == 0) {
				finish_time = std::chrono::system_clock::now();
				elapsed = finish_time - start_time;
				std::cout << " " << thousandths / 10 << "% (" << bad_count << " bad)" << std::endl;
			}
		}
		auto[d, closed] = write_queue.pop();
		out << d.first << " " << d.second << "\n";
		if (d.second.none()) {
			bad << d.first << " " << d.second << std::endl;
			bad_count++;
		}
		matrices_ready++;
	}

	out.close();
	bad.close();
	std::cout << bad_count << " bad out of " << matrices_total << " matrices\n";

	check_future.wait();

	return 0;
}

