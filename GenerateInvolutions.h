#pragma once

#include <deque>

#include "Consts.h"

namespace gi {

size_t generate(std::deque<bit_dump>& out, size_t th_count);

}